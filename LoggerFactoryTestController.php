<?php

namespace Koala\TestController;

use Koala\ControllerExtender\ControllerBase;
use Silex\Application;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LoggerFactoryTestController extends ControllerBase {

	public function test(Request $request) {
		/** @var Logger $logger */
		$logger = $this->app['logger.factory']->getLogger( 'test' );
		$entry = $request->get('log') == null ? 'test log entry' : $request->get('log');
		$logger->log( Logger::DEBUG , $entry );
		return new Response();
	}

}