<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 20.04.2015
 * Time: 14:13
 */

namespace Koala\MonologExtender\LoggerFactory;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerFactory {

    public $loggers ;

    function addLogger(  $name, $pathToSave, $warningLevel = Logger::DEBUG )
    {
        $log = new Logger( $name );
        $log->pushHandler(new StreamHandler( $pathToSave, $warningLevel ) ) ;
        $this->loggers[ $name ] = $log;
    }

    function getLogger( $name )
    {
        return $this->loggers[ $name ];
    }
} 