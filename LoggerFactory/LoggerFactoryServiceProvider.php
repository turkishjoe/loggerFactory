<?php

namespace Koala\MonologExtender\LoggerFactory;

use Silex\Application;
use Silex\ServiceProviderInterface ;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LoggerFactoryServiceProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     */
    public function register(Application $app)
    {
      //  $app['data.logs'] = array();

            $app[ 'logger.factory' ] = $app->share(
				function() use ($app) {
					$loggerFactory = new LoggerFactory();
					foreach ($app['logFiles'] as $key => $value) {        // create a log channel
						$path = $app['app.logs.dir'] . $value;
						$loggerFactory->addLogger( $key, $path, Logger::DEBUG );
					}
					return $loggerFactory;
            	}
			);
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     */
    public function boot(Application $app)
    {
        // TODO: Implement boot() method.
    }
}